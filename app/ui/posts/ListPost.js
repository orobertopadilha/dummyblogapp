import React, { useCallback, useEffect, useState } from 'react'
import { FlatList} from 'native-base'
import { SafeAreaView } from 'react-native'

import postsService from '../../service/PostsService'
import { useNavigation } from '@react-navigation/native'
import ListPostItem from './ListPostItem'
import { ROUTE_POST_DETAILS } from '../../navigation/AppRoutes'

const ListPost = () => {
    const [posts, setPosts] = useState([])
    const navigation = useNavigation()

    useEffect(() => {
        const loadPosts = async () => {
            let result = await postsService.findAll()
            setPosts(result)
        }
        
        loadPosts()
    }, [])

    const openDetails = useCallback((item) => {
        navigation.navigate(ROUTE_POST_DETAILS, item)
    }, [])

    const renderPostItem = useCallback( ({item}) => 
        <ListPostItem item={item} onItemSelected={openDetails} />
    , [])

    const itemKeyExtractor = useCallback((item) => item.id, [])

    return (
        <SafeAreaView>                
            <FlatList 
                data={posts}
                renderItem={renderPostItem}
                keyExtractor={itemKeyExtractor}/>
        </SafeAreaView>
    )
}

export default ListPost;
