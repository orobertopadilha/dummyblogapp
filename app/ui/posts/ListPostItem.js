import React from 'react'
import { Box, Heading, Pressable, Text } from "native-base"
import CardView from '../../components/CardView'

const ListPostItem = ({item, onItemSelected}) => {

    return (
        <Pressable onPress={() => onItemSelected(item)}>
            <CardView>
                <Heading size="md">{item.title}</Heading>
                <Text fontSize="xs" marginTop={2}>{item.body}</Text>
            </CardView>
        </Pressable>
    )
}

export default ListPostItem