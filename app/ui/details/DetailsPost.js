import { Text } from 'react-native'
import React, { useCallback, useEffect, useState } from 'react'
import { useRoute } from '@react-navigation/core'
import { FlatList, Heading, VStack } from 'native-base'
import CommentItem from './CommentItem'
import { SafeAreaView } from 'react-native-safe-area-context'
import postsService from '../../service/PostsService'

const DetailsPost = () => {
    const [comments, setComments] = useState([])

    const route = useRoute()
    const postItem = route.params

    useEffect(() => {
        const loadComments = async () => {
            let result = await postsService.getComments(postItem.id)
            setComments(result)
        }

        loadComments()
    }, [])

    const renderComment = useCallback( ({ item }) => <CommentItem comment={item}/>, [])

    const commentKeyExtractor = useCallback( item => item.id, [])

    return (
        <SafeAreaView flex={1}>
            <VStack space={2} margin={4}>
                <Heading size="lg">{postItem.title}</Heading>
                <Text>{postItem.body}</Text>
                <Heading size="sm" marginTop={3}>Comments</Heading>
            </VStack>
            <FlatList 
                flex={1}
                data={comments}
                renderItem={renderComment}
                keyExtractor={commentKeyExtractor}
            />
        </SafeAreaView>
    )
}

export default DetailsPost