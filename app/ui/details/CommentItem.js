import { Box, Text } from 'native-base'
import React from 'react'
import CardView from '../../components/CardView'

const CommentItem = ({ comment }) => {
    
    return (
        <CardView>
            <Text bold>{comment.name}</Text>
            <Text fontSize="xs">{comment.email}</Text>
            <Text marginTop={3} fontSize="sm">{comment.body}</Text>
        </CardView>
    )
}

export default CommentItem