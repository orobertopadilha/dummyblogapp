import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { StatusBar, useTheme } from 'native-base'
import React from 'react'
import DetailsPost from '../ui/details/DetailsPost'
import ListPost from '../ui/posts/ListPost'
import { ROUTE_HOME, ROUTE_POST_DETAILS } from './AppRoutes'

const Stack = createNativeStackNavigator()

const AppNavigation = () => {

    const { colors } = useTheme()
    const blueColor = colors.blue

    const screenOptions = {
        headerStyle: {
            backgroundColor: blueColor["700"]
        },
        headerTintColor: colors.white
    }

    return (
        <NavigationContainer>
            <StatusBar backgroundColor={blueColor["800"]} />
            <Stack.Navigator screenOptions={screenOptions}>
                <Stack.Screen name={ROUTE_HOME} component={ListPost}
                    options={{ title: 'Blog' }} />
                <Stack.Screen name={ROUTE_POST_DETAILS} component={DetailsPost}
                    options={{ title: 'Post Details' }} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default AppNavigation