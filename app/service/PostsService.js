import { http } from "./ServiceProvider"

const findAll = async () => {
    let result = await http.get("posts")
    return result.data
}

const getComments = async (postId) => {
    let result = await http.get(`posts/${postId}/comments`)
    return result.data
}

const postsService = { findAll, getComments }

export default postsService