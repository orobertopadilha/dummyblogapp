import axios from "axios"

const setupAxios = () => {
    let instance = axios.create({
        baseURL: 'https://jsonplaceholder.typicode.com/'
    })

    return instance
}

const http = setupAxios()

export { http }